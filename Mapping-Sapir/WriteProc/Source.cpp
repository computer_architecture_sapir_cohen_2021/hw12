#include <Windows.h>
#include <stdio.h>

#define FILENAME "C:\\Users\\Sapir\\gitArch\\hw12\\gibrish.bin"
#define WRITE_LETTER 'Z'

int main()
{
	HANDLE hMapFile;
	LPTSTR pBuf;

	hMapFile = OpenFileMappingA(FILE_MAP_ALL_ACCESS, TRUE, "mymap");

	pBuf = (LPTSTR)MapViewOfFile(hMapFile, // handle to map object
		FILE_MAP_WRITE,  // write permission
		0,
		0,
		1);

	if(pBuf != NULL) *pBuf = WRITE_LETTER;

	UnmapViewOfFile(pBuf);
	CloseHandle(hMapFile);
}