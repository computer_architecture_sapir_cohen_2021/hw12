#include <Windows.h>
#include <iostream>

#define FILENAME "C:\\Sapir\\Magshimim\\2\\windows\\hw12\\gibrish.bin"
#define SEARCH_LETTER 'Z'

int main()
{
	HANDLE hFile;
	LPCSTR pFileName = FILENAME;
	hFile = CreateFileA(pFileName, // file name
		GENERIC_READ | GENERIC_WRITE, // access type
		FILE_SHARE_WRITE, 
		NULL,
		OPEN_EXISTING, // open only if file exists
		FILE_ATTRIBUTE_NORMAL,
		NULL);

	if (hFile == NULL) std::cout << "hFile error: " << GetLastError() << std::endl;

	HANDLE hMapFile;
	hMapFile = CreateFileMappingA(hFile, // file handle
		NULL, // default security
		PAGE_READWRITE, // read access
		0, // maximum object size (high-order DWORD)
		0, // maximum object size (low-order DWORD)
		"mymap");

	if (hMapFile == NULL) std::cout << "hMapFile error: " << GetLastError() << std::endl;
	
	LPSTR pBuf;
	char letter = ';';

	while (letter != SEARCH_LETTER)
	{
		pBuf = (LPSTR)MapViewOfFile(hMapFile, // handle to map object
			FILE_MAP_READ, // read/write permission
			0, // start point (upper word)
			0, // start point (lower word)
			1); // how many bytes to read
		
		if(pBuf == NULL) std::cout << "pBuf error: " << GetLastError() << std::endl;

		letter = *pBuf;
		UnmapViewOfFile(pBuf);
	}

	std::cout << "first letter became: " << *(pBuf) << std::endl;

	CloseHandle(hMapFile);
	CloseHandle(hFile);

	return 0;
}
