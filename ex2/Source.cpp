#include <Windows.h>
#include <iostream>

#define SIZE 2

wchar_t arg[SIZE][100] = { L"C:\\Sapir\\Magshimim\\2\\windows\\hw12\\Mapping-Sapir\\x64\\Debug\\readProc.exe", 
								L"C:\\Sapir\\Magshimim\\2\\windows\\hw12\\Mapping-Sapir\\x64\\Debug\\WriteProc.exe" };

int main()
{
	PROCESS_INFORMATION process_info[SIZE] = { 0 };
	STARTUPINFO process_startup_info[SIZE] = { 0 };

	for (int i = 0; i < SIZE; i++)
	{
		ZeroMemory(&process_startup_info[i], sizeof(process_startup_info[i]));
		process_startup_info[i].cb = sizeof(process_startup_info[i]);
		ZeroMemory(&process_info[i], sizeof(process_info[i]));
	}
	
	for (int i = 0; i < SIZE; i++)
	{
		if (!CreateProcess(
			NULL, 
			arg[i],  // Command line
			NULL,
			NULL,
			FALSE,
			0,
			NULL,
			NULL,
			&process_startup_info[i],
			&process_info[i]))
		{
			std::cout << "process " << i << " failed" << std::endl;
			return 1;
		}
	}

	for (int i = 0; i < SIZE; i++)
	{
		WaitForSingleObject(process_info[i].hProcess, INFINITE);
		CloseHandle(process_info[i].hThread);
		CloseHandle(process_info[i].hProcess);
	}

}